﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace ChessServer
{
    class Program
    {
        static List<Client[]> clientList = new List<Client[]>();
        static void Main(string[] args)
        {
            clientList.Add(new Client[2]);
            IPAddress ipAd = Dns.GetHostEntry("localhost").AddressList[0];
            int port = 8080;
            Ask(ref ipAd, ref port);
            TcpListener server = new TcpListener(ipAd, port);
            server.Start();
            Console.WriteLine("\nWaiting for a connection...");
            while (true)
            {
                Socket socket = server.AcceptSocket();
                Console.WriteLine("\nAccepted connection from {0}...", socket.RemoteEndPoint);
                AddToList(socket);
            }
        }
        static void Ask(ref IPAddress ip, ref int port)
        {
            while (true)
            {
                Console.WriteLine("\nUse localhost:8080? y/n");
                string ans = Console.ReadLine();
                if (ans == "y")
                {

                    ip = Dns.GetHostEntry("localhost").AddressList[0];
                    port = 8080;
                    return;
                }
                else if (ans == "n")
                {
                    while (true)
                    {
                        Console.WriteLine("\nThis machine's ip? y/n");
                        ans = Console.ReadLine();
                        if (ans == "y")
                        {
                            ip = Array.Find(
                            Dns.GetHostEntry(string.Empty).AddressList,
                            a => a.AddressFamily == AddressFamily.InterNetwork);
                            Console.WriteLine("\nEnter port:");
                            port = Int32.Parse(Console.ReadLine());
                            return;
                        }
                        else if (ans == "n")
                        {
                            Console.WriteLine("\nEnter ip:");
                            ip = IPAddress.Parse(Console.ReadLine());
                            Console.WriteLine("\nEnter port:");
                            port = Int32.Parse(Console.ReadLine());
                            return;

                        }
                    }
                }
            }
        }

        static void AddToList(Socket socket)
        {
            Color color;
            Console.WriteLine("connected {0}", clientList.Count());
            if(clientList.Count() == 1 || clientList.Last()[1] != null)
            {
                Random rngColor = new Random();
                int rng = rngColor.Next(1, 11);
                if (rng % 2 == 0)
                    color = Color.White;
                else
                    color = Color.Black;
                Console.WriteLine("random color: {0}",color);
                clientList.Add(new Client[2]);
                clientList.Last()[0] = new Client(socket, color);
            }
            else
            {
                color = clientList.Last()[0].color == Color.White ? Color.Black : Color.White;
                clientList.Last()[0].gamePair = clientList.Last();
                clientList.Last()[1] = new Client(socket, color)
                {
                    gamePair = clientList.Last()
                };
                Task.Factory.StartNew(() =>
                {
                    clientList.Last()[0].Listen();
                });
                Task.Factory.StartNew(() =>
                {
                    clientList.Last()[1].Listen();
                });

            }
        }
    }

    class Client
    {
        public Color color;
        public Socket socket;
        public Client[] gamePair;
        public Client(Socket socket, Color color)
        {
            this.socket = socket;
            this.color = color;
        }
        public void Listen()
        {
            bool firstRun = true;
            while (true)
            {
                byte[] receiveBuffer = new byte[1024];
                UnicodeEncoding enc = new UnicodeEncoding();
                try
                {
                    if (firstRun)
                    {
                        Console.WriteLine("\n{0} is {1}", socket.RemoteEndPoint, color.ToString());
                        socket.Send(enc.GetBytes(color.ToString()));
                        firstRun = false;
                    }
                    int bytesRec = socket.Receive(receiveBuffer);
                    Array.Resize(ref receiveBuffer, bytesRec);
                    Console.WriteLine(Encoding.Unicode.GetString(receiveBuffer));
                    foreach (Client client in gamePair)
                    {
                        if(client.socket != this.socket)
                        {
                            client.socket.Send(receiveBuffer);
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("{0} disconnected", socket.RemoteEndPoint);
                    try
                    {
                        foreach (Client client in gamePair)
                        {
                            if (client.socket != this.socket)
                            {
                                client.socket.Send(enc.GetBytes("Forfeit"));
                            }
                        }
                    }
                    catch { }
                    return;
                }
            }
        }
    }
    enum Color
    {
        White,
        Black
    }
}
